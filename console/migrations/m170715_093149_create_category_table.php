<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
/**
 * Console comand
 * yii migrate/create create_category_table --fields="url:string(255):notNull:unique,name:string(255):notNull,content:text,short_content:string(255),meta_title:string(255),meta_description:string(255),meta_keywords:string(255),state:smallInteger(1):notNull:defaultValue(1),position:integer(11):notNull:defaultValue(0),image:string(255),created_date:dateTime,updated_date:dateTime"
  */
class m170715_093149_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'url' => $this->string(255)->notNull()->unique(),
            'name' => $this->string(255)->notNull(),
            'content' => $this->text(),
            'short_content' => $this->string(255),
            'meta_title' => $this->string(255),
            'meta_description' => $this->string(255),
            'meta_keywords' => $this->string(255),
            'state' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'position' => $this->integer(11)->notNull()->defaultValue(0),
            'image' => $this->string(255),
            'created_date' => $this->dateTime(),
            'updated_date' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category');
    }
}
