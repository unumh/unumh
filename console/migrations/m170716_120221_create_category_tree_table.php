<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category_tree`.
 */
class m170716_120221_create_category_tree_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category_tree', [
            'id' => $this->primaryKey(),
            'parentId' => $this->integer(11)->notNull(),
            'subId' => $this->integer(11)->notNull(),
        ]);
        $this->createIndex('index_unique_parent_sub_category',
                'category_tree', 
                ['parentId', 'subId'], 
                true);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category_tree');
    }
}
