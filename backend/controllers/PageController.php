<?php

namespace backend\controllers;

use Yii;
use backend\models\Page;
use backend\models\PageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\UploadImageForm;
use yii\web\UploadedFile;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Page models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new PageSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Page model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Page model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$page = new Page();
		$uploadImage = new UploadImageForm();

		if ($page->load(Yii::$app->request->post())) {
			$uploadImage->imageFile = UploadedFile::getInstance($uploadImage, 'imageFile');
			if ($uploadImage->upload(lcfirst($page->formName()))) {
				$page->image = $uploadImage->path;
			}
			$page->save();
			return $this->redirect(['view', 'id' => $page->id]);
		} else {
			return $this->render('create', [
				'model' => $page,
				'uploadImage' => $uploadImage,
			]);
		}
	}

	/**
	 * Updates an existing Page model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$page = $this->findModel($id);
		$uploadImage = new UploadImageForm();

		if ($page->load(Yii::$app->request->post())) {
			$uploadImage->imageFile = UploadedFile::getInstance($uploadImage, 'imageFile');
			if ($uploadImage->upload(lcfirst($page->formName()))) {
				$page->image = $uploadImage->path;
			}
			$page->save();
			return $this->redirect(['view', 'id' => $page->id]);
		} else {
			return $this->render('update', [
				'model' => $page,
				'uploadImage' => $uploadImage,
			]);
		}
	}

	/**
	 * Deletes an existing Page model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Page model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Page the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Page::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
	/*
		public function actionPageForm()
		{
			$model = new backend\models\Page(['scenario' => 'Empty']);

			if ($model->load(Yii::$app->request->post())) {
				if ($model->validate()) {
					// form inputs are valid, do something here
					return;
				}
			}

			return $this->render('PageForm', [
				'model' => $model,
			]);
		}*/
}
