<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $parentId
 * @property string $name
 * @property string $title
 * @property string $content
 * @property string $shortContent
 * @property string $url
 * @property integer $state
 * @property integer $position
 * @property string $image
 * @property string $createDate
 * @property string $publishDate
 */
class Page extends \yii\db\ActiveRecord
{
	/**
	 *
	 */
	const STATE_ACTIVE = 1;
	/**
	 *
	 */
	const STATE_DELETE = 2;

	/**
	 * @var array
	 */
	protected $_stateAlias = [
		self::STATE_ACTIVE => 'Active',
		self::STATE_DELETE => 'Delete'
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'page';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['parentId', 'state', 'position'], 'integer'],
			[['name', 'title', 'content', 'shortContent', 'url'], 'required'],
			[['content', 'shortContent'], 'string'],
			[['createDate', 'publishDate'], 'safe'],
			[['name', 'url', 'image'], 'string', 'max' => 255],
			[['title'], 'string', 'max' => 500],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'parentId' => 'Parent ID',
			'name' => 'Name',
			'title' => 'Title',
			'content' => 'Content',
			'shortContent' => 'Short Content',
			'url' => 'Url',
			'state' => 'State',
			'position' => 'Position',
			'image' => 'Image',
			'createDate' => 'Create Date',
			'publishDate' => 'Publish Date',
		];
	}

	/**
	 * @return bool
	 */
	public function delete()
	{
		$this->state = self::STATE_DELETE;
		return $this->update(true);
	}

	/**
	 * @return mixed
	 */
	public function getStateAlias()
	{
		return $this->_stateAlias[$this->state];
	}
}
