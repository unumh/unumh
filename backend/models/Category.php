<?php

namespace backend\models\Category;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $url
 * @property string $name
 * @property string $content
 * @property string $short_content
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $state
 * @property integer $position
 * @property string $image
 */
class Category extends \yii\db\ActiveRecord
{
	/**
	 *
	 */
	const STATE_ACTIVE = 1;
	/**
	 *
	 */
	const STATE_DELETE = 2;

	/**
	 * @var array
	 */
	protected $_stateAlias = [
		self::STATE_ACTIVE => 'Active',
		self::STATE_DELETE => 'Delete'
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'category';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['url', 'name'], 'required'],
			[['content'], 'string'],
			[['state', 'position'], 'integer'],
			[['url', 'name', 'short_content', 'meta_title', 'meta_description', 'meta_keywords', 'image'], 'string', 'max' => 255],
			[['url'], 'unique'],
			[['position'], 'default', 'value' => 1],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'url' => 'Url',
			'name' => 'Name',
			'content' => 'Content',
			'short_content' => 'Short Content',
			'meta_title' => 'Meta Title',
			'meta_description' => 'Meta Description',
			'meta_keywords' => 'Meta Keywords',
			'state' => 'State',
			'position' => 'Position',
			'image' => 'Image',
		];
	}

	/**
	 * @return bool
	 */
	public function delete()
	{
		$this->state = self::STATE_DELETE;
		return $this->update(true);
	}

	/**
	 * @return mixed
	 */
	public function getStateAlias()
	{
		return $this->_stateAlias[$this->state];
	}
}
