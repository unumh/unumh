<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "page-form">

	<?php
	$form = ActiveForm::begin();

	echo $form->field($model, 'parentId')->textInput(['value' => 0]);
	echo $form->field($model, 'name')->textInput(['maxlength' => true]);
	echo $form->field($model, 'title')->textInput(['maxlength' => true]);
	echo $form->field($model, 'content')->textarea(['rows' => 6]);
	echo $form->field($model, 'shortContent')->textarea(['rows' => 6]);
	echo $form->field($model, 'url')->textInput(['maxlength' => true]);
	echo $form->field($model, 'position')->textInput(['value' => 1]);

	echo Html::img($model->image);
	echo $form->field($uploadImage, 'imageFile')->fileInput();
	?>

	<div class = "form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
