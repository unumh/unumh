<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class = "page-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Create Page', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?php Pjax::begin();
	echo GridView::widget([
		'dataProvider' => $dataProvider,
//		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'parentId',
//			'name',
			'title',
			'shortContent:ntext',
			// 'content:ntext',
			// 'url:url',
			// 'state',
			// 'position',
			// 'image',
			// 'publishDate:date',
			'createDate:date',
			[
				'attribute' => 'state',
				'content' => function ($data) {
						return $data->getStateAlias();
					}
			],

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
	<?php Pjax::end(); ?></div>
