<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Category */
/* @var $form ActiveForm */
?>
<div class="category">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'url') ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'content') ?>
        <?= $form->field($model, 'state') ?>
        <?= $form->field($model, 'position') ?>
        <?= $form->field($model, 'short_content') ?>
        <?= $form->field($model, 'meta_title') ?>
        <?= $form->field($model, 'meta_description') ?>
        <?= $form->field($model, 'meta_keywords') ?>
        <?= $form->field($model, 'image') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- category -->
