<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\File;

/* @var $this yii\web\View */
/* @var $model backend\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "category-form">

	<?php
	$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);

	echo $form->field($model, 'name')->textInput(['maxlength' => true]);
	echo $form->field($model, 'url')->textInput(['maxlength' => true]);
	echo $form->field($model, 'content')->textarea(['rows' => 6]);
	echo $form->field($model, 'short_content')->textInput(['maxlength' => true]);
	echo $form->field($model, 'meta_title')->textInput(['maxlength' => true]);
	echo $form->field($model, 'meta_description')->textInput(['maxlength' => true]);
	echo $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]);
	echo $form->field($model, 'state')->dropDownList([
		'1' => 'Активный',
		'2' => 'Удален'
	]);
	echo $form->field($model, 'position')->textInput(['value' => 1]);

	echo Html::img($model->image);
	echo $form->field($uploadImage, 'imageFile')->fileInput();

	?>
	<div class = "form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
