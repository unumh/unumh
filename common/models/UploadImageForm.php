<?php
/**
 * Created by PhpStorm.
 * User: doctor
 * Date: 7/18/17
 * Time: 8:38 PM
 */

namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class UploadImageForm extends Model
{
	/**
	 * @var UploadedFile
	 */
	public $imageFile;
	public $path = '';

	public function rules()
	{
		return [
			[['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
		];
	}

	public function upload($savePath = '', $fileName = '')
	{
		if (empty($fileName)) {
			$fileName = self::_generateFileHashName();
		}
		if ($this->validate()) {
			if (FileHelper::createDirectory('uploads/' . $savePath, 0777)) {
				$path = 'uploads/' . $savePath . '/' . $fileName . '.' . $this->imageFile->extension;
				if ($this->imageFile->saveAs($path)) {
					$this->path = $path;
				} else {
					return false;
				}
			} else {
				return false;
			}
			
			return true;
		} else {
			return false;
		}
	}

	static function _generateFileHashName()
	{
		return 'img_' . md5(time());
	}
}