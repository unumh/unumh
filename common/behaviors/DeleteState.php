<?php
/**
 * Created by PhpStorm.
 * User: doctor
 * Date: 7/18/17
 * Time: 7:30 PM
 */

namespace common\behaviors;

use yii;
use yii\base\Behavior;


class DeleteState extends Behavior
{
	public $iniciali = null;

	public function events()
	{
		return [
			yii\web\Controller::EVENT_BEFORE_ACTION => 'getMyMetod'
		];
	}

	public function getMyMetod()
	{
		yii::$app->params['fio'] = $this->iniciali . "ksl";
	}

} 